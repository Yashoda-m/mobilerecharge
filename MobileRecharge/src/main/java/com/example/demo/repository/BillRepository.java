package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.entity.Bill;

public interface BillRepository extends JpaRepository<Bill,Long> {

}
