
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="com.example.demo.entity.Offer"%>
   
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Bill page</title>
</head>
<body>
<% Offer o = (Offer) request.getAttribute("savedplan");  %>
bill for Re-Charge on mobile Number : ${phonenumber}<br><br>
Transaction-Id : ${transactionId}<br><br>
valid for: <%=o.getDays() %><br><br>
Talk Time : <%=o.getTalkTime() %><br><br>
Data balance : <%=o.getData() %><br><br>
Amount Payable : <%=o.getPrice() %><br><br>

<a href="/" >Return</a>
</body>
</html>