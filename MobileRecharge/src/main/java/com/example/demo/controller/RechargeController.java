package com.example.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Bill;
import com.example.demo.entity.Offer;
import com.example.demo.repository.BillRepository;
import com.example.demo.repository.OfferRepository;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Bill;
import com.example.demo.entity.Offer;
import com.example.demo.repository.BillRepository;
import com.example.demo.repository.OfferRepository;

@Controller
public class RechargeController {
	
	@Autowired
	OfferRepository offerRepository;
	
	@Autowired
	BillRepository billRepository;

	
	
	@RequestMapping("/")
	public String home() {
		
		return"index";
	}
	
	@PostMapping("showPlans")
	public ModelAndView showDetails(@RequestParam("operator") Integer operatorId,@RequestParam("phonenumber") Long phoneNumber ) {
		
		ModelAndView mv = new ModelAndView("plandetails");
		mv.addObject("offerslists",offerRepository.findByOperatorId(operatorId));
		mv.addObject("phonenumber",phoneNumber );
		return mv;
		
	}
	
	@PostMapping("savePlan")
	public ModelAndView saveDetails(@RequestParam("planId") Integer planId,@RequestParam("phonenumber") Long phoneNumber ) {
		
		ModelAndView mv = new ModelAndView("Bill");
		Offer obj = offerRepository.findById(planId).orElse(new Offer());
		Bill  bill = new Bill();
		bill.setPhoneNumber(phoneNumber);
		bill.setPlanId(planId);
		billRepository.save(bill);
		mv.addObject("savedplan",obj);
		mv.addObject("phonenumber",phoneNumber );
		return mv;
	}
}
	
	
	


