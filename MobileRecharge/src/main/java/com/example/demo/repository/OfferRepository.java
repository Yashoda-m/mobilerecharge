package com.example.demo.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.example.demo.entity.Offer;
import com.example.demo.entity.Operator;

public interface OfferRepository extends JpaRepository<Offer,Integer> {
	
	@Query(value = "select * from plandata o where o.operator_operator_id =:operatorId",nativeQuery = true)
	List<Offer> findByOperatorId(@Param("operatorId") int operator);
	
	
	
}
