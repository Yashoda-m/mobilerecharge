<%@ page import="com.example.demo.entity.Offer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>plan Page</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<% List<Offer> offersList = (List<Offer>) request.getAttribute("offerslists");%>   
 <% Long phoneNumber = (Long) request.getAttribute("phonenumber");%>

    <h2 style="text-align: center; color: blue;"> Select Plan From Below</h2><br><br>
    <div class = "container">
    <table class = "table">
        <tr>
        <th></th>
        <th>Plan Id</th>
        <th>days</th>
        <th>TalkTime</th>
        <th>Data</th>
        <th>Price</th>
         </tr>
  <tr>
    <% for(Offer o: offersList) { %>
        <form action = "savePlan" method="post">
        <tr>
            <td>
            <input type="radio"  name="planId" value=<%= o.getPlanId() %>>
            </td>
            <td>
                <% out.print(o.getPlanId()); %>
            </td>
            <td>
                <% out.print(o.getDays()); %>
            </td>
            <td>
                <% out.print(o.getTalkTime()); %>
            </td>
            <td>
                <% out.print(o.getData()); %>
            </td>
            <td>
                <% out.print(o.getPrice()); %>
            </td>



       </tr>
    <% } %>    
        
    </table>
      <input type="hidden"  name="phonenumber" value=<%=phoneNumber %>>
      <input type = "submit" value="Submit">
    </form>
</body>
    
    </div>
    
    
    


	
</body>
</html>